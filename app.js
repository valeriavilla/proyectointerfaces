const app = new Vue({

	el: '#app',
	data: {
		titulo: 'Comentarios',
		tareas: [],
		nuevaTarea: '',
		category: 'marvel'
	},
	methods:{
		agregarTarea: function(){
			this.tareas.push({
				nombre: new Date().getDate() + "/" + (new Date().getMonth() + 1) + "/" + new Date().getFullYear() + ": " + this.nuevaTarea
			});
			this.nuevaTarea = '';
		},
		eliminar: function(index){
			this.tareas.splice(index,1);
		},
		set_category: function(option) {
			this.category = option
		}
	}
	
});